package com.eliseev.curve.util

import android.animation.Animator
import android.animation.AnimatorInflater
import android.content.Context
import android.support.annotation.AnimatorRes

open class AnimatorLoader(private val context: Context) {

    open fun loadAnimator(@AnimatorRes id: Int): Animator {
        return AnimatorInflater.loadAnimator(context, id)
    }
}
