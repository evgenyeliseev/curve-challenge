package com.eliseev.curve.di

import android.content.Context
import com.eliseev.curve.BuildConfig
import com.eliseev.curve.R
import com.eliseev.curve.sum.SumViewModel
import com.eliseev.curve.sum.blink.AnimatorBlink
import com.eliseev.curve.sum.blink.Blink
import com.eliseev.curve.sum.blink.HandlerBlink
import com.eliseev.curve.sum.blink.RxJavaBlink
import com.eliseev.curve.util.AnimatorLoader
import io.reactivex.android.schedulers.AndroidSchedulers
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import java.util.Random

val viewModelModule: Module = applicationContext {

    bean { SumViewModel(BuildConfig.NUM_EDITORS) }
}

val animModule: Module = applicationContext {

    bean { createBlinkAnim(get()) }
}

private fun createBlinkAnim(context: Context): Blink {
    val interval = context.resources.getInteger(R.integer.blink_interval).toLong()
    return when (Random(System.currentTimeMillis()).nextInt(3)) {
        0 -> AnimatorBlink(AnimatorLoader(context), interval)
        1 -> HandlerBlink(interval)
        else -> RxJavaBlink(AndroidSchedulers.mainThread(), interval)
    }
}
