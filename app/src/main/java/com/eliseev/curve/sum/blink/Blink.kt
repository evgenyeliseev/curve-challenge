package com.eliseev.curve.sum.blink

import android.view.View

abstract class Blink protected constructor(protected val interval: Long) {

    abstract fun start(target: View)
    abstract fun stop()
}
