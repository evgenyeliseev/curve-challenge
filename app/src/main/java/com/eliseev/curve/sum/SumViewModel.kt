package com.eliseev.curve.sum

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.ObservableField

open class SumViewModel(numValues: Int) : BaseObservable() {

    @Bindable
    val sum = ObservableField<String>("0")

    @Bindable
    val values = IntArray(numValues, { 0 })

    @Bindable
    fun getTextListener(): (Int, String) -> Unit {
        return ::onInputChanged
    }

    private fun onInputChanged(fieldIndex: Int, text: String) {
        if (text.isNotEmpty()) {
            values[fieldIndex] = text.toInt()
            sum.set(values.sum().toString())
        }
    }
}
