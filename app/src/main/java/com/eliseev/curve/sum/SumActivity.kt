package com.eliseev.curve.sum

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.eliseev.curve.R
import com.eliseev.curve.databinding.ActivityMainBinding
import com.eliseev.curve.sum.blink.Blink
import kotlinx.android.synthetic.main.activity_main.editorsList
import kotlinx.android.synthetic.main.activity_main.sum
import org.koin.android.ext.android.inject

class SumActivity : AppCompatActivity() {

    private val viewModel: SumViewModel by inject()
    private val blink: Blink by inject()

    private var isBlinking = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = viewModel

        sum.setOnClickListener {
            if (isBlinking) blink.stop() else blink.start(sum)
            isBlinking = !isBlinking
        }

        editorsList.adapter = EditorsAdapter(viewModel)
    }

    override fun onResume() {
        super.onResume()
        if (isBlinking) blink.start(sum)
    }

    override fun onPause() {
        super.onPause()
        blink.stop()
    }
}
