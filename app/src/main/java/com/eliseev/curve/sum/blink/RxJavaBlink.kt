package com.eliseev.curve.sum.blink

import android.view.View
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class RxJavaBlink(private val observerScheduler: Scheduler, interval: Long) : Blink(interval) {

    @Volatile
    private var started = false

    override fun start(target: View) {
        started = true
        Observable
                .interval(interval, TimeUnit.MILLISECONDS)
                .takeWhile {
                    started
                }
                .observeOn(observerScheduler)
                .subscribeBy(
                        onNext = {
                            target.alpha = if (target.alpha == 0f) 1f else 0f
                        },
                        onComplete = {
                            target.alpha = 1f
                        }
                )
    }

    override fun stop() {
        started = false
    }
}
