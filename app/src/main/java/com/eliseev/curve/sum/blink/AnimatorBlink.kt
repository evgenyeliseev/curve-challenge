package com.eliseev.curve.sum.blink

import android.animation.Animator
import android.view.View
import androidx.animation.doOnCancel
import com.eliseev.curve.R
import com.eliseev.curve.util.AnimatorLoader

class AnimatorBlink(private val animLoader: AnimatorLoader, interval: Long) : Blink(interval) {

    private var anim: Animator? = null

    override fun start(target: View) {
        anim = animLoader.loadAnimator(R.animator.blink)
        anim?.apply {
            setTarget(target)
            duration = interval * 2
            setInterpolator {
                if (it >= 0.5) 1f else 0f
            }
            doOnCancel {
                target.alpha = 1f
            }
            start()
        }
    }

    override fun stop() {
        anim?.cancel()
    }
}
