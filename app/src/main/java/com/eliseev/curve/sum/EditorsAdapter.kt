package com.eliseev.curve.sum

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import com.eliseev.curve.databinding.EditorLayoutBinding

class EditorsAdapter(private val viewModel: SumViewModel) : RecyclerView.Adapter<EditorsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = EditorLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        binding.viewModel = viewModel
        return ViewHolder(binding)
    }

    override fun getItemCount() = viewModel.values.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.id = position
        holder.binding.executePendingBindings()

        (holder.itemView as EditText).apply {
            id = position
            post {
                if (hasFocus()) {
                    selectAll()
                }
            }
        }
    }

    class ViewHolder(val binding: EditorLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}
