package com.eliseev.curve.sum

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.FocusFinder
import android.view.View

@Suppress("unused")
/**
 * This class fixes GridLayoutManager's focus search when navigating from keyboard
 */
class EditorsLayoutManager(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
        GridLayoutManager(context, attrs, defStyleAttr, defStyleRes) {

    private val focusFinder = FocusFinder.getInstance()
    private var recyclerView: RecyclerView? = null

    override fun onAttachedToWindow(view: RecyclerView?) {
        super.onAttachedToWindow(view)
        recyclerView = view
    }

    override fun onDetachedFromWindow(view: RecyclerView?, recycler: RecyclerView.Recycler?) {
        recyclerView = null
        super.onDetachedFromWindow(view, recycler)
    }

    override fun onInterceptFocusSearch(focused: View, direction: Int): View? {
        return recyclerView?.let {
            focusFinder.findNextFocus(recyclerView, focused, View.FOCUS_FORWARD)
        }
    }
}
