package com.eliseev.curve.sum.blink

import android.os.Handler
import android.view.View

class HandlerBlink(interval: Long) : Blink(interval) {

    private val handler = Handler()
    private var started = false

    override fun start(target: View) {
        started = true
        doNext(target)
    }

    override fun stop() {
        started = false
    }

    private fun doNext(target: View) {
        handler.postDelayed({
            if (started) {
                target.alpha = if (target.alpha == 0f) 1f else 0f
                doNext(target)
            } else {
                target.alpha = 1f
            }
        }, interval)
    }
}
