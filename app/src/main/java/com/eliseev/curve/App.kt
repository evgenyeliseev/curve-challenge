package com.eliseev.curve

import android.app.Application
import com.eliseev.curve.di.animModule
import com.eliseev.curve.di.viewModelModule
import org.koin.android.ext.android.startKoin

@Suppress("unused")
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(viewModelModule, animModule))
    }
}
