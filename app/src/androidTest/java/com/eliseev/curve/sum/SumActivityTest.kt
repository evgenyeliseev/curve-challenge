package com.eliseev.curve.sum

import android.app.Application
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import com.eliseev.curve.BuildConfig
import com.eliseev.curve.R
import com.eliseev.curve.di.viewModelModule
import com.eliseev.curve.sum.blink.Blink
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.android.ext.koin.with
import org.koin.dsl.module.applicationContext
import org.koin.standalone.StandAloneContext.closeKoin
import org.koin.standalone.StandAloneContext.startKoin
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.quality.Strictness

class SumActivityTest {

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS)

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(SumActivity::class.java, true, false)

    @Mock
    private lateinit var blink: Blink

    private val mockAnimModule = applicationContext {

        bean { blink }
    }

    @Before
    fun setUp() {
        closeKoin()
        startKoin(listOf(viewModelModule, mockAnimModule)) with
                InstrumentationRegistry.getTargetContext().applicationContext as Application
        activityRule.launchActivity(null)
    }

    @Test
    fun updatesSum() {
        var sum = 0
        (0 until BuildConfig.NUM_EDITORS).forEach {
            sum += it
            onView(withId(it)).perform(typeText(it.toString()))
            onView(withId(R.id.sum)).check(matches(withText(sum.toString())))
        }
    }

    @Test
    fun sumStartsBlinkingOnTouch() {
        onView(withId(R.id.sum)).perform(click())
        verify(blink).start(any())
    }

    @Test
    fun sumStopsBlinkingOnSecondTouch() {
        onView(withId(R.id.sum)).perform(click())
        onView(withId(R.id.sum)).perform(click())
        verify(blink).stop()
    }

    @Test
    fun retainsValuesOnConfigurationChange() {
        var sum = 0
        (0 until BuildConfig.NUM_EDITORS).forEach {
            sum += it
            onView(withId(it)).perform(typeText(it.toString()))
        }

        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            activityRule.activity.recreate()
        }

        (0 until BuildConfig.NUM_EDITORS).forEach {
            onView(withId(it)).check(matches(withText(it.toString())))
        }
        onView(withId(R.id.sum)).check(matches(withText(sum.toString())))
    }
}
