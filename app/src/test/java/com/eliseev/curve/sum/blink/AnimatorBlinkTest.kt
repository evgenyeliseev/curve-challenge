package com.eliseev.curve.sum.blink

import android.animation.Animator
import android.view.View
import com.eliseev.curve.util.AnimatorLoader
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.quality.Strictness

class AnimatorBlinkTest {

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS)

    @Mock
    private lateinit var view: View

    @Mock
    private lateinit var animLoader: AnimatorLoader

    @Mock
    private lateinit var animator: Animator

    private lateinit var blink: AnimatorBlink

    @Before
    fun setUp() {
        whenever(animLoader.loadAnimator(any())).thenReturn(animator)
        blink = AnimatorBlink(animLoader, 0)
    }

    @Test
    fun startsAnimation() {
        blink.start(view)
        verify(animator).setTarget(view)
        verify(animator).start()
    }

    @Test
    fun stopsAnimation() {
        blink.start(view)

        val argumentCaptor = argumentCaptor<Animator.AnimatorListener>()
        verify(animator).addListener(argumentCaptor.capture())

        blink.stop()
        verify(animator).cancel()

        argumentCaptor.firstValue.onAnimationCancel(animator)
        verify(view).alpha = 1f
    }
}