package com.eliseev.curve.sum.blink

import android.view.View
import com.nhaarman.mockito_kotlin.atLeastOnce
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.schedulers.Schedulers
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.quality.Strictness
import java.util.concurrent.Exchanger
import java.util.concurrent.Executor

class RxJavaBlinkTest {

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS)

    @Mock
    private lateinit var view: View

    private val executor = BlockingExecutor()
    private val blink = RxJavaBlink(Schedulers.from(executor), 0)

    @Test
    fun changesToInvisible() {
        whenever(view.alpha).thenReturn(1f)
        blink.start(view)
        executor.await()
        verify(view, atLeastOnce()).alpha = 0f
    }

    @Test
    fun changesToVisible() {
        whenever(view.alpha).thenReturn(0f)
        blink.start(view)
        executor.await()
        verify(view, atLeastOnce()).alpha = 1f
    }

    @Test
    fun stopsWhenRequested() {
        whenever(view.alpha).thenReturn(1f)
        blink.start(view)
        executor.await()
        blink.stop()
        executor.await()
        verify(view).alpha = 1f
    }

    class BlockingExecutor : Executor {

        private val exchanger = Exchanger<Unit>()

        override fun execute(command: Runnable) {
            command.run()
            exchanger.exchange(Unit)
        }

        fun await() {
            exchanger.exchange(Unit)
        }
    }
}