package com.eliseev.curve.sum

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class SumViewModelTest {

    private val viewModel = SumViewModel(10)
    private lateinit var textListener: (Int, String) -> Unit

    @Before
    fun setUp() {
        textListener = viewModel.getTextListener()
    }

    @Test
    fun updatesSumFromOneEditor() {
        textListener(0, "2")
        assertEquals("2", viewModel.sum.get())

        textListener(0, "5")
        assertEquals("5", viewModel.sum.get())
    }

    @Test
    fun updatesSumFromMultipleEditors() {
        textListener(0, "2")
        textListener(5, "33")
        assertEquals("35", viewModel.sum.get())

        textListener(0, "3")
        textListener(5, "15")
        assertEquals("18", viewModel.sum.get())
    }

    @Test
    fun doesNotChangeSumOnEmptyInput() {
        textListener(0, "3")
        textListener(0, "")
        assertEquals("3", viewModel.sum.get())
    }
}